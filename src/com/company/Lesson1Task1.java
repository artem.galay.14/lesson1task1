package com.company;

import java.io.IOException;

/**
 * Lesson1Task1.
 * ДЗ 1, Задача 1
 *
 * @author Artem_Galay
 */

public class Lesson1Task1 {
    static Object a = null;
    static Object b = null;
    static int[] c = new int[3];

    // метод, бросающий NullPointerException
    static void npeThrows() throws NullPointerException {
        System.out.println("Throwing NPE...");
        System.out.println(a.equals(b));
    }

    // метод, бросающий ArrayIndexOutOfBoundsException
    static void outOfBoundThrows() throws ArrayIndexOutOfBoundsException {
        System.out.println("Throwing ArrayIndexOutOfBoundsException...");
        c[4] = 10;
    }

    // метод, бросающий IOException
    static void myExceptionThrows() throws IOException {
        System.out.println("Throwing IOException...");
        throw new IOException();
    }

    public static void main(String[] args) {
	// write your code here 2
        // бросаем NullPointerException
        try {
            npeThrows();
        } catch (NullPointerException e) {
            System.out.println("NullPointerException22");
        }

        // бросаем ArrayIndexOutOfBoundsException
        try {
            outOfBoundThrows();
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("ArrayIndexOutOfBoundsException");
        }

        // бросаем IOException
        try {
            myExceptionThrows();
        } catch (IOException e) {
            System.out.println("IOException");
        }
    }
}
